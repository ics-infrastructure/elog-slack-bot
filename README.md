# elog-slack-bot

This repository includes two applications:

- *slack-bot*: a webserver that responds to the /elog slack command
- *elog-bot*: a bot that polls the logbook server to import new messages to an elasticsearch database
  and post them to a slack channel

## Quickstart

To run the applications locally:

1. Create the `.env` file with the following variables:

   ```
   SLACK_API_TOKEN=token
   SLACK_SIGNING_SECRET=secret
   SLACK_CHANNEL=channel
   ELOG_LOGBOOK=logbook-name
   ELOG_USERNAME=user
   ELOG_PASSWORD=password
   ELOG_URL=https://logbook.example.org
   ELASTICSEARCH_URL=http://localhost:9200
   ```

   The `ELOG_PASSWORD` shall be already encrypted.

2. Build the docker image:

   ```
   $ docker-compose build
   ```

3. Run and let start elasticsearch in the background

   ```
   $ docker-compose up -d elasticsearch
   ```

4. Create the elasticsearch index and import all messages from elog.
   This will drop the index if it already exists. This is only required once.

   ```
   $ docker-compose run --rm elog-bot elog-bot reimport
   ```

5. Run both applications

   ```
   $ docker-compose up elog-bot slack-bot
   ```

6. To stop everything

   ```
   $ docker-compose down
   ```

## slack-bot

To use the slack-bot, you should create a Slack app and define the /elog slash command.
Refer to https://api.slack.com/interactivity/slash-commands

## elog-bot

The elog-bot allows you to run the following commands:

   ```
   $ docker-compose run --rm elog-bot elog-bot --help

   Usage: elog-bot [OPTIONS] COMMAND [ARGS]...

   Options:
     --version  Show the version and exit.
     --help     Show this message and exit.

   Commands:
     create-index  Create the elasticsearch index
     delete-index  Delete the elasticsearch index
     import-new    Import new messages (without slack notification) from the...
     poll          Poll the logbook to import new messages (with slack...
     reimport      Reimport all messages from the logbook to elasticsearch (no...
     remove        Remove a document from the index
   ```

## License

MIT
