FROM python:3.7-slim as base

# Install Python dependencies in an intermediate image
# as some requires a compiler (uvloop)
FROM base as builder

# Install dependencies required to compile some Python packages
# Taken from https://github.com/docker-library/python/blob/master/3.7/stretch/slim/Dockerfile
RUN apt-get update \
  && apt-get install -yq --no-install-recommends \
  && apt-get update && apt-get install -y --no-install-recommends \
    dpkg-dev \
    gcc \
    libbz2-dev \
    libc6-dev \
    libexpat1-dev \
    libffi-dev \
    libgdbm-dev \
    liblzma-dev \
    libncursesw5-dev \
    libreadline-dev \
    libsqlite3-dev \
    libssl-dev \
    make \
    tk-dev \
    uuid-dev \
    wget \
    xz-utils \
    zlib1g-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /requirements.txt
RUN python -m venv /venv \
  && . /venv/bin/activate \
  && pip install --no-cache-dir -r /requirements.txt

FROM base

RUN groupadd -r -g 1000 csi \
  && useradd --no-log-init -r -g csi -u 1000 csi

COPY --chown=csi:csi --from=builder /venv /venv
COPY --chown=csi:csi . /app/
WORKDIR /app

ENV PATH /venv/bin:$PATH

RUN pip install .

USER csi
