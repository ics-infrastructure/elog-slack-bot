from setuptools import setup, find_packages

requirements = [
    "fastapi",
    "python-multipart",
    "uvicorn",
    "gunicorn",
    "httpx",
    "html2text",
    "elasticsearch",
    "slackclient",
    "elog",
]

test_requirements = ["pytest>=3.0.0", "pytest-cov"]

setup(
    name="elog-slack-bot",
    author="Benjamin Bertrand",
    author_email="benjamin.bertrand@esss.se",
    description="Bot to interact with elog via slack",
    url="https://gitlab.esss.lu.se/ics-infrastructure/elog-slack-bot",
    license="MIT license",
    version="0.1.0",
    install_requires=requirements,
    tests_require=test_requirements,
    packages=find_packages(),
    include_package_data=True,
    keywords="slack",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    entry_points={"console_scripts": ["elog-bot=elog_slack_bot.cli:cli"]},
    extras_require={"tests": test_requirements},
)
