from starlette.config import environ

environ["SLACK_API_TOKEN"] = "slack-api-token"
environ["SLACK_SIGNING_SECRET"] = "slack-signing-secret"
environ["SLACK_CHANNEL"] = "mychannel"
environ["ELOG_URL"] = "https://elog.example.org"
environ["ELOG_USERNAME"] = "elog-user"
environ["ELOG_PASSWORD"] = "elog-password"
environ["ELASTICSEARCH_URL"] = "http://localhost:9200"
