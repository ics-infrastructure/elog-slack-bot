from starlette.testclient import TestClient
from elog_slack_bot.bot import bot


client = TestClient(bot.app)


def test_bot_health():
    response = client.get("/health")
    assert response.status_code == 200
    assert response.json() == "Bot OK"
