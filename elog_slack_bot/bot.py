from .slack_bot import SlackBot
from . import elog_cmd

bot = SlackBot()
bot.register_commands(elog_cmd.cmd)
app = bot.app
