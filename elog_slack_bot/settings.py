from starlette.config import Config
from starlette.datastructures import Secret

config = Config(".env")

SLACK_API_TOKEN = config("SLACK_API_TOKEN", cast=Secret, default="token")
SLACK_SIGNING_SECRET = config("SLACK_SIGNING_SECRET", cast=Secret, default="secret")
SLACK_CHANNEL = config("SLACK_CHANNEL", cast=str, default="mychannel")
ELOG_LOGBOOK = config("ELOG_LOGBOOK", cast=str, default="Operation")
ELOG_URL = config("ELOG_URL", cast=str, default="http://logbook.example.org")
ELOG_LOGBOOK_URL = f"{ELOG_URL}/{ELOG_LOGBOOK}"
ELOG_USERNAME = config("ELOG_USERNAME", cast=str, default="user")
ELOG_PASSWORD = config("ELOG_PASSWORD", cast=Secret, default="password")
ELASTICSEARCH_URL = config(
    "ELASTICSEARCH_URL", cast=str, default="http://localhost:9200"
)
# Shall only be set to "true" for testing to make
# documents visible for search immediately
# https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-refresh.html
ELASTICSEARCH_REFRESH = False
