from typing import Callable, Dict
import hmac
import hashlib
import logging
from time import time
from fastapi import FastAPI, APIRouter, HTTPException, BackgroundTasks
from fastapi.routing import APIRoute
from starlette.responses import Response
from starlette.requests import Request
from .utils import send_text
from .settings import SLACK_SIGNING_SECRET

logger = logging.getLogger(__name__)


class SlackCommand:
    """Class to store subcommands and associated functions"""

    def __init__(self, name: str):
        self.name = name
        self._subcommands: Dict[str, Callable] = {}

    @property
    def subcommands(self):
        return list(self._subcommands.keys())

    def add(self, func: Callable, subcommand: str) -> None:
        """Add a subcommand and its associated function"""
        self._subcommands[subcommand] = func

    def register(self, subcommand: str):
        """A decorator that calls :meth:`add` on the decorated function.

        .. code-block:: python

            cmd = slack_bot.SlackCommand("/mycmd")

            @cmd.register("help")
            async def cmd_help(subcmd_text, response_url, **kwargs):
                ...
        """

        def decorator(func: Callable) -> Callable:
            self.add(func, subcommand)
            return func

        return decorator

    async def dispatch(self, subcommand: str, **kwargs: str) -> None:
        """Call the function associated to the *subcommand*"""
        try:
            await self._subcommands[subcommand](**kwargs)
        except KeyError as e:
            logger.warning(f"Unknown subcommand {subcommand}.")
            logger.warning(e)
            await send_text(
                kwargs["response_url"], text=f"Unknown subcommand {subcommand}"
            )


class SlackValidationRoute(APIRoute):
    """API Route class to verify requests from slack"""

    def get_route_handler(self):
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            await verify_slack_request(request)
            return await original_route_handler(request)

        return custom_route_handler


async def verify_slack_request(request: Request):
    """Verify slack request timestamp and signature

    This function ensures the request came from Slack as
    recommended https://api.slack.com/docs/verifying-requests-from-slack
    Will return a 400 or 403 if the validation fails.
    """
    try:
        timestamp = request.headers["X-Slack-Request-Timestamp"]
    except KeyError:
        raise HTTPException(
            status_code=400, detail="Missing X-Slack-Request-Timestamp header"
        )
    if abs(time() - int(timestamp)) > 60 * 5:
        # The request timestamp is more than five minutes from local time.
        # It could be a replay attack
        raise HTTPException(status_code=403, detail="Invalid request timestamp")
    body = await request.body()
    req_basestring = str.encode(f"v0:{timestamp}:") + body
    signature = (
        "v0="
        + hmac.new(
            str.encode(str(SLACK_SIGNING_SECRET)), req_basestring, hashlib.sha256
        ).hexdigest()
    )
    try:
        slack_signature = request.headers["X-Slack-Signature"]
    except KeyError:
        raise HTTPException(status_code=400, detail="Missing X-Slack-Signature header")
    if not hmac.compare_digest(signature, slack_signature):
        raise HTTPException(status_code=403, detail="Invalid request signature")


class SlackBot:
    """A SlackBot is a FastAPI webserver that handles Slack commands

    Typical usage is::

        from slack_bot import SlackBot, SlackCommand

        bot = SlackBot()
        cmd = SlackCommand("/mycmd")

        @cmd.register("help")
        async def mycmd_help(subcmd_text, response_url):
            send_text(response_url, "help message")


        bot.register_commands(cmd)


        uvicorn main:bot.app
    """

    def __init__(self):
        self.app = FastAPI()
        self.app.add_api_route(path="/health", endpoint=self.health, methods=["GET"])
        # Specific router to verify requests from Slack
        slack_router = APIRouter(route_class=SlackValidationRoute)
        slack_router.add_api_route(
            path="/slack/cmd", endpoint=self.slack_command, methods=["POST"]
        )
        self.app.include_router(slack_router)

    async def health(self):
        """Handler to check that the bot is alive"""
        return "Bot OK"

    async def slack_command(self, request: Request, background_tasks: BackgroundTasks):
        """Handler to receive slack commands

        Commands are dispatched to the registered commands and subcommands
        with the parameters from the form plus the subcmd_text
        extracted from the text field

        The form fields should include:
        token=gIkuvaNzQIHg97ATvDxqgjtO
        team_id=T0001
        team_domain=example
        enterprise_id=E0001
        enterprise_name=Globular%20Construct%20Inc
        channel_id=C2147483705
        channel_name=test
        user_id=U2147483697
        user_name=Steve
        command=/weather
        text=94070
        response_url=https://hooks.slack.com/commands/1234/5678
        trigger_id=13345224609.738474920.8088930838d88f008e0
        """
        form = await request.form()
        subcommand, _, subcmd_text = form.get("text", "").partition(" ")
        if subcommand == "":
            subcommand = "help"
        # The command will be run in the background after we sent an empty HTTP 200
        # to acknowledge we received the payload
        background_tasks.add_task(
            self.dispatch, subcommand=subcommand, subcmd_text=subcmd_text, **form
        )
        logger.debug(f"Slack command received: {form}")
        return Response(status_code=200)

    def register_commands(self, *commands: SlackCommand):
        """Register a list of :class:`SlackCommand`"""
        if hasattr(self, "_commands"):
            raise TypeError(
                "Some commands are already registered."
                "'register_commands' can only be called when no command is registered."
            )
        self._commands = {cmd.name: cmd for cmd in commands}

    async def dispatch(self, command: str, subcommand: str, **kwargs):
        """Call the function associated to the *command* and *subcommand*"""
        try:
            await self._commands[command].dispatch(subcommand, **kwargs)
        except KeyError:
            logger.warning(f"Unknown command {command}")
            await send_text(kwargs["response_url"], text=f"Unknown command {command}")
