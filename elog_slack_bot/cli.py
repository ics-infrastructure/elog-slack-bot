import click
import logging
import time
import slack
from .elastic_logbook import ElasticLogbook
from . import utils
from .settings import (
    ELASTICSEARCH_URL,
    ELOG_URL,
    ELOG_LOGBOOK,
    ELOG_USERNAME,
    ELOG_PASSWORD,
    SLACK_API_TOKEN,
    SLACK_CHANNEL,
)


logbook = ElasticLogbook(
    ELASTICSEARCH_URL, ELOG_URL, ELOG_LOGBOOK, ELOG_USERNAME, str(ELOG_PASSWORD)
)
slack_client = slack.WebClient(token=str(SLACK_API_TOKEN))


def post_to_slack(message):
    blocks = utils.format_message_for_slack(message)
    slack_client.chat_postMessage(channel=SLACK_CHANNEL, blocks=blocks)
    utils.upload_attachments(message, SLACK_CHANNEL, logbook)


@click.group()
@click.version_option()
def cli():
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )


@cli.command()
def poll():
    """Poll the logbook to import new messages (with slack notification)"""
    while True:
        try:
            logbook.import_new_messages(func=post_to_slack)
        except Exception as e:
            logging.warning(e)
            time.sleep(1)
            continue
        time.sleep(5)


@cli.command()
def reimport():
    """Reimport all messages from the logbook to elasticsearch (no slack notification)"""
    logbook.reimport()


@cli.command()
def import_new():
    """Import new messages (without slack notification) from the logbook to elasticsearch"""
    logbook.import_new_messages()


@cli.command()
def create_index():
    """Create the elasticsearch index"""
    logbook.create_index()


@cli.command()
def delete_index():
    """Delete the elasticsearch index"""
    logbook.delete_index()


@cli.command()
@click.argument("id")
def remove(id):
    """Remove a document from the index"""
    logbook.remove_from_index(id)


if __name__ == "__main__":
    cli()
