import io
import itertools
import logging
import html2text
import httpx
import requests
from .settings import ELOG_LOGBOOK_URL, SLACK_API_TOKEN

logger = logging.getLogger(__name__)


async def send_slack_message(
    response_url, text=None, blocks=None, response_type="ephemeral"
):
    """Send a slack message to a response URL"""
    data = {"response_type": response_type}
    if text is not None:
        data = {"text": text}
    elif blocks is not None:
        data = {"blocks": blocks}
    await httpx.post(response_url, json=data)


async def send_text(url, text, response_type="ephemeral"):
    """Send a slack text message to a response URL"""
    await send_slack_message(url, text=text, response_type=response_type)


def upload_attachments(message, channels, logbook):
    """Upload all message attachments"""
    data = {"token": SLACK_API_TOKEN, "channels": channels}
    for attachment in logbook.get_attachments(message):
        data["filename"] = attachment.filename
        requests.post(
            "https://slack.com/api/files.upload",
            files={"file": io.BytesIO(attachment.content)},
            data=data,
        )


async def send_logbook_messages(
    url, messages, channels, logbook, response_type="ephemeral"
):
    """Send a list of elog messages to a Slack response URL"""
    blocks = format_messages_for_slack(messages)
    await send_slack_message(url, blocks=blocks, response_type=response_type)
    for message in messages:
        upload_attachments(message, channels, logbook)


def logbook_link(id):
    return f"<{ELOG_LOGBOOK_URL}/{id}|{id}>"


def markdown_block(text):
    return {"type": "section", "text": {"type": "mrkdwn", "text": text}}


def get_markdown_header(message):
    """Return the message header as markdown"""
    header = [f"*{logbook_link(message['id'])}: {message['subject']}*"]
    in_reply_to = ", ".join(
        [f"{logbook_link(id)}" for id in message.get("in_reply_to", [])]
    )
    if in_reply_to:
        header.append(f"*In reply to*: {in_reply_to}")
    for key in ["date", "author", "entry_type", "category"]:
        header.append(f"*{key}*: {message[key]}")
    return "\n".join(header)


def get_markdown_content(message):
    """Return the message content as markdown"""
    parser = html2text.HTML2Text()
    parser.mark_code = True
    content = parser.handle(message["html_content"])
    content = content.replace("[code]", "```")
    content = content.replace("[/code]", "```")
    return content


def get_markdown_attachments(message):
    """Return the message attchments as markdown"""
    if message.get("attachments"):
        return "*Attachments*:\n * " + "\n * ".join(message["attachments"])
    return ""


def format_messages_for_slack(messages):
    """Format a list of messages in blocks for slack"""
    return list(
        itertools.chain.from_iterable(
            [format_message_for_slack(message) for message in messages]
        )
    )


def format_message_for_slack(message):
    """Format a message in blocks for slack"""
    header = get_markdown_header(message)
    content = get_markdown_content(message)
    attachments = get_markdown_attachments(message)
    blocks = [markdown_block(header), {"type": "divider"}, markdown_block(content)]
    if attachments:
        blocks.append(markdown_block(attachments))
    blocks.append({"type": "divider"})
    return blocks
