from .slack_bot import SlackCommand
from .elastic_logbook import ElasticLogbook
from . import utils
from .settings import (
    ELASTICSEARCH_URL,
    ELOG_URL,
    ELOG_LOGBOOK,
    ELOG_USERNAME,
    ELOG_PASSWORD,
)


logbook = ElasticLogbook(
    ELASTICSEARCH_URL, ELOG_URL, ELOG_LOGBOOK, ELOG_USERNAME, str(ELOG_PASSWORD)
)
cmd = SlackCommand("/elog")


@cmd.register("help")
async def help(response_url, **kwargs):
    text = """/elog command:
     /elog get <message id> [<message id>...]
     /elog search <query>
     /elog last [nb]
     """
    await utils.send_text(response_url, text)


@cmd.register("get")
async def get(subcmd_text, response_url, channel_id, **kwargs):
    """Return messages by id

    Several ids can be passed by parameter
    """
    if subcmd_text in ("", "help"):
        text = "/elog get <message id>"
        await utils.send_text(response_url, text)
    else:
        messages = [logbook.get_document(id) for id in subcmd_text.strip().split()]
        await utils.send_logbook_messages(response_url, messages, channel_id, logbook)


@cmd.register("search")
async def search(subcmd_text, response_url, channel_id, **kwargs):
    if subcmd_text in ("", "help"):
        text = """/elog search <query>
        /elog search EMU
        /elog search subject:EMU AND author:marc
        /elog search date:2019-03-15
        /elog search date:[20190315 TO 20190330]
        """
        await utils.send_text(response_url, text)
        return
    messages = logbook.search(subcmd_text, sort="date:desc", per_page=1)
    if not messages:
        await utils.send_text(response_url, f"No messages found for '{subcmd_text}'")
        return
    await utils.send_logbook_messages(response_url, messages, channel_id, logbook)


@cmd.register("last")
async def last(subcmd_text, response_url, channel_id, **kwargs):
    if subcmd_text == "help":
        text = """/elog last [nb]
        /elog last -> return last message
        /elog last 3 -> return last 3 messages
        No more than 10 messages can be requested
        """
        await utils.send_text(response_url, text)
        return
    try:
        per_page = min(10, int(subcmd_text))
    except ValueError:
        per_page = 1
    messages = logbook.search("*", sort="date:desc", per_page=per_page)
    await utils.send_logbook_messages(response_url, messages, channel_id, logbook)
