import logging
import time
import elog
import requests
from datetime import datetime
from collections import namedtuple
from elasticsearch import Elasticsearch, helpers
from elasticsearch.exceptions import NotFoundError
from .settings import ELASTICSEARCH_REFRESH

logger = logging.getLogger(__name__)
Attachment = namedtuple("Attachment", "filename content")

# 'Date': 'Wed, 09 Oct 2019 21:36:05 +0200',
# 'In reply to': '1692',
# 'Author': 'Ryoichi Miyamoto',
# 'Session ID': '20190702A',
# 'Entry Type': 'Note',
# 'Entry Class': 'Note',
# 'Category': 'Operation',
# 'Insert Template': 'No',
# 'Section': 'LEBT',
# 'Discipline': 'BMD',
# 'Subject': 'LEBT chopper width scan',
# 'Encoding': 'HTML'
MESSAGE_MAPPING = {
    "id": {"type": "integer"},
    "date": {
        "type": "date",
        "format": "strict_date_optional_time||yyyy-MM-dd||yyyy-MM||yyyyMMdd||yyyyMM",
    },
    "reply_to": {"type": "integer"},
    "in_reply_to": {"type": "integer"},
    "author": {"type": "text", "fields": {"keyword": {"type": "keyword"}}},
    "session_id": {"type": "text", "fields": {"keyword": {"type": "keyword"}}},
    "shift_id": {"type": "text", "fields": {"keyword": {"type": "keyword"}}},
    "entry_type": {"type": "text", "fields": {"keyword": {"type": "keyword"}}},
    "entry_class": {"type": "text", "fields": {"keyword": {"type": "keyword"}}},
    "category": {"type": "text", "fields": {"keyword": {"type": "keyword"}}},
    "insert_template": {"type": "boolean"},
    "section": {"type": "text", "fields": {"keyword": {"type": "keyword"}}},
    "discipline": {"type": "text", "fields": {"keyword": {"type": "keyword"}}},
    "subject": {"type": "text", "fields": {"keyword": {"type": "keyword"}}},
    "encoding": {"type": "text", "fields": {"keyword": {"type": "keyword"}}},
    "html_content": {"type": "text", "analyzer": "html_strip_analyzer"},
    "attachments": {"type": "text"},
}


class ElasticLogbook:
    def __init__(
        self, elastic_url, logbook_url, logbook_name, logbook_user, logbook_password
    ):
        self.logbook_name = logbook_name
        self.index = logbook_name.lower()
        self._elasticsearch = Elasticsearch(elastic_url)
        self._logbook = elog.open(f"{logbook_url}/{logbook_name}", user=logbook_user)
        self._logbook._password = logbook_password

    def delete_index(self, **kwargs):
        """Delete the elasticsearch index"""
        self._elasticsearch.indices.delete(index=self.index, **kwargs)

    def create_index(self, **kwargs):
        """Create the elasticsearch index"""
        body = {
            "settings": {
                "analysis": {
                    "analyzer": {
                        "html_strip_analyzer": {
                            "type": "custom",
                            "tokenizer": "standard",
                            "char_filter": ["html_strip"],
                        }
                    }
                }
            },
            "mappings": {"dynamic": "strict", "properties": MESSAGE_MAPPING},
        }
        self._elasticsearch.indices.create(index=self.index, body=body, **kwargs)

    def get_message(self, message_id):
        """Return a message from the logbook by id"""
        content, attributes, attachments = self._logbook.read(message_id)
        id = attributes.pop("$@MID@$")
        document = {
            key.lower().replace(" ", "_"): value for key, value in attributes.items()
        }
        document["id"] = int(id)
        # Convert date to ISO format
        # 'Date': 'Wed, 09 Oct 2019 21:36:05 +0200',
        document["date"] = datetime.strptime(
            document["date"], "%a, %d %b %Y %H:%M:%S %z"
        ).isoformat()
        # Convert to bool
        document["insert_template"] = document["insert_template"] != "No"
        # Convert in_reply_to and reply_to to list
        if "in_reply_to" in document:
            document["in_reply_to"] = [
                int(id)
                for id in document["in_reply_to"].strip().split(",")
                if id.strip()
            ]
        if "reply_to" in document:
            document["reply_to"] = [
                int(id) for id in document["reply_to"].strip().split(",") if id.strip()
            ]
        document["html_content"] = content
        if len(attachments) == 1 and attachments[0].endswith("/"):
            document["attachments"] = []
        else:
            document["attachments"] = attachments
        return document

    def add_to_index(self, body):
        """Add a message to the index"""
        # Using the message id as document id allows us to easily retrieve it by id
        self._elasticsearch.index(
            index=self.index, id=body["id"], body=body, refresh=ELASTICSEARCH_REFRESH
        )

    def remove_from_index(self, id):
        """Remove the document id from the index"""
        self._elasticsearch.delete(
            index=self.index, id=id, refresh=ELASTICSEARCH_REFRESH
        )

    def get_document(self, id):
        """Return a document from elasticsearch by id"""
        try:
            return self._elasticsearch.get_source(self.index, id)
        except NotFoundError:
            message = self.get_message(id)
            self.add_to_index(message)
            return message

    def get_document_ids(self):
        """Return all document ids from elasticsearch index as set"""
        query = {"_source": False, "query": {"match_all": {}}}
        scan = helpers.scan(self._elasticsearch, index=self.index, query=query)
        return {int(doc["_id"]) for doc in scan}

    def get_message_ids(self):
        """Return all message ids from the logbook as set"""
        return set(self._logbook.get_message_ids())

    def import_new_messages(self, func=None):
        """Import new messages from the logbook into elasticsearch

        If func is given, it will be called with each message imported
        """
        new_ids = self.get_message_ids() - self.get_document_ids()
        for msg_id in new_ids:
            logger.info(f"Importing message {msg_id}")
            try:
                message = self.get_message(msg_id)
            except elog.logbook_exceptions.LogbookServerProblem as e:
                logger.warning(e)
                # Give some time to the elog server...
                time.sleep(1)
                continue
            else:
                self.add_to_index(message)
                if func is not None:
                    func(message)

    def reimport(self):
        """Force to re-import all messages from the logbook to elasticsearch

        The index is deleted and recreated
        """
        self.delete_index(ignore_unavailable=True)
        self.create_index()
        self.import_new_messages()

    def search(self, query, page=1, per_page=5, sort=None):
        """Run a search query on elasticsearch index

        Return a list of documents
        """
        kwargs = {
            "index": self.index,
            "q": query,
            "from_": (page - 1) * per_page,
            "size": per_page,
        }
        if sort is not None:
            kwargs["sort"] = sort
        result = self._elasticsearch.search(**kwargs)
        total_hits = (result["hits"]["total"]["value"],)
        logger.info(f"total hits: {total_hits}")
        return [doc["_source"] for doc in result["hits"]["hits"]]

    def get_attachment(self, url):
        """Return the file name and and content as namedtuple"""
        response = requests.get(
            url,
            headers={"Cookie": self._logbook._make_user_and_pswd_cookie()},
            allow_redirects=False,
            verify=False,
        )
        filename = url.split("/")[-1]
        return Attachment(filename, response.content)

    def get_attachments(self, message):
        """Return all message attchments"""
        return [self.get_attachment(url) for url in message["attachments"]]
